# The “Time Select” Kata

This is a programming kata I made up for myself. I use it to practice TDD in Rust.

> Represent a time of day, with methods offsetting it by 1 hour, 1 minute, or half a day (i.e. flipping AM/PM). Methods must correctly wrap around midnight. Include a way of obtaining the time value in 12-hour format.

I also use it as back-end to testing various GUI frameworks. For example, see [front-end made with Yew](https://gitlab.com/mojicfg/time-select-yew) for a better understanding of what this kata is supposed to achieve.

## Summary

This kata is simple & lightweight. It should be a quick exercise, especially upon repetitions.

My solution in this repo:

- basic Rust, no external crates
- made by following TDD principles
- a single `lib.rs` file with tests included
- uses `cargo test` for testing
- introduces a simple macro creating a neat DSTL which makes me happy
