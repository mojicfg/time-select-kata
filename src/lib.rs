#[cfg(feature = "serde")]
use serde::*;

#[macro_export]
macro_rules! time {
    ($h:literal:$m:literal am) => {
        Time::from(Time12hFormat {
            hours: $h,
            minutes: $m,
            am_pm: Am,
        })
    };
    ($h:literal:$m:literal pm) => {
        Time::from(Time12hFormat {
            hours: $h,
            minutes: $m,
            am_pm: Pm,
        })
    };
}

#[derive(Clone, Debug, Default, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Time(u32);
impl Time {
    pub fn into_next_hour(self) -> Self {
        self.into_offset_in_minutes(60)
    }
    pub fn into_previous_hour(self) -> Self {
        self.into_offset_in_minutes(-60)
    }
    pub fn into_next_minute(self) -> Self {
        self.into_offset_in_minutes(1)
    }
    pub fn into_previous_minute(self) -> Self {
        self.into_offset_in_minutes(-1)
    }
    fn into_offset_in_minutes(self, offset: i32) -> Self {
        let minutes_in_a_day = 24 * 60;
        let original_minutes = self.0 as i32;
        let offset_minutes = original_minutes + offset;
        let wrapped_minutes = offset_minutes.rem_euclid(minutes_in_a_day);
        Self(wrapped_minutes as u32)
    }
    pub fn into_am_pm_flipped(self) -> Self {
        let original: Time12hFormat = self.into();
        let flipped = Time12hFormat {
            am_pm: match original.am_pm {
                Am => Pm,
                Pm => Am,
            },
            ..original
        };
        flipped.into()
    }
}

#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Time12hFormat {
    pub hours: u32,
    pub minutes: u32,
    pub am_pm: AmPm,
}
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum AmPm {
    Am,
    Pm,
}
pub use AmPm::*;

impl From<Time12hFormat> for Time {
    fn from(f: Time12hFormat) -> Self {
        assert!(f.hours <= 11);
        assert!(f.minutes <= 59);
        let am_pm_offset = match f.am_pm {
            Am => 0,
            Pm => 12 * 60,
        };
        Self::default()
            .into_offset_in_minutes(f.hours as i32 * 60)
            .into_offset_in_minutes(f.minutes as i32)
            .into_offset_in_minutes(am_pm_offset)
    }
}

impl Into<Time12hFormat> for Time {
    fn into(self) -> Time12hFormat {
        let full_hours = self.0 / 60;
        let minutes_within_current_hour = self.0 % 60;
        Time12hFormat {
            hours: full_hours % 12,
            minutes: minutes_within_current_hour,
            am_pm: if full_hours < 12 { Am } else { Pm },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn default_is_midnight() {
        let time = Time::default();
        assert_eq!(time!(00:00 am), time);
    }
    #[test]
    fn creating() {
        let time = time!(04:20 am);
        assert_eq!(time!(04:20 am), time);
    }
    #[test]
    #[should_panic]
    fn cannot_create_over_11_hours() {
        let _time = time!(12:00 am);
    }
    #[test]
    #[should_panic]
    fn cannot_create_over_59_minutes() {
        let _time = time!(00:60 am);
    }

    macro_rules! test {
        { $name:ident
            given    $original:expr,
            invoking .$method:ident
            creates  $expected:expr
        } => {
            #[test]
            fn $name() {
                let original = $original;
                let actual = original.$method();
                assert_eq!($expected, actual);
            }
        };
    }

    test! { into_next_hour
        given    time!(04:20 am),
        invoking .into_next_hour
        creates  time!(05:20 am)
    }
    test! { into_next_hour_wraps_day_around
        given    time!(11:16 pm),
        invoking .into_next_hour
        creates  time!(00:16 am)
    }

    test! { into_previous_hour
        given    time!(00:04 pm),
        invoking .into_previous_hour
        creates  time!(11:04 am)
    }
    test! { into_previous_hour_wraps_day_around
        given    time!(00:09 am),
        invoking .into_previous_hour
        creates  time!(11:09 pm)
    }

    test! { into_next_minute
        given    time!(08:43 am),
        invoking .into_next_minute
        creates  time!(08:44 am)
    }
    test! { into_next_minute_wraps_day_around
        given    time!(11:59 pm),
        invoking .into_next_minute
        creates  time!(00:00 am)
    }

    test! { into_previous_minute
        given    time!(05:43 pm),
        invoking .into_previous_minute
        creates  time!(05:42 pm)
    }
    test! { into_previous_minute_wraps_day_around
        given    time!(00:00 am),
        invoking .into_previous_minute
        creates  time!(11:59 pm)
    }

    test! { into_12h_format_am
        given    time!(02:24 am),
        invoking .into
        creates  Time12hFormat {
            hours: 2,
            minutes: 24,
            am_pm: Am,
        }
    }
    test! { into_12h_format_pm
        given    time!(08:28 pm),
        invoking .into
        creates  Time12hFormat {
            hours: 8,
            minutes: 28,
            am_pm: Pm,
        }
    }

    test! { from_12h_format_am
        given Time12hFormat {
            hours: 9,
            minutes: 30,
            am_pm: Am,
        },
        invoking .into
        creates  time!(09:30 am)
    }
    test! { from_12h_format_pm
        given Time12hFormat {
            hours: 6,
            minutes: 45,
            am_pm: Pm,
        },
        invoking .into
        creates  time!(06:45 pm)
    }

    test! { am_flips_to_pm
        given    time!(03:33 am),
        invoking .into_am_pm_flipped
        creates  time!(03:33 pm)
    }
    test! { pm_flips_to_am
        given    time!(05:55 pm),
        invoking .into_am_pm_flipped
        creates  time!(05:55 am)
    }
}
